import { Component } from '@angular/core';
import * as firebase from "firebase";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {

    const firebaseConfig = {
      apiKey: "AIzaSyC3IvoUd26ggH8iSSFlvtPzowk5N8uNIuk",
      authDomain: "tutosangular.firebaseapp.com",
      databaseURL: "https://tutosangular.firebaseio.com",
      projectId: "tutosangular",
      storageBucket: "tutosangular.appspot.com",
      messagingSenderId: "124311630723",
      appId: "1:124311630723:web:68a34476990d2b9eef7cb6",
      measurementId: "G-ZR0RFJHHCQ"
    };
    firebase.initializeApp(firebaseConfig);
    //firebase.analytics();
  }
}
